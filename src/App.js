import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <code> _    _      _ _        __          __        _     _ </code>
        <code>| |  | |    | | |       \ \        / /       | |   | |</code>
        <code>| |__| | ___| | | ___    \ \  /\  / /__  _ __| | __| |</code>
        <code>|  __  |/ _ \ | |/ _ \    \ \/  \/ / _ \| '__| |/ _` |</code>
        <code>| |  | |  __/ | | (_) |    \  /\  / (_) | |  | | (_| |</code>
        <code>|_|  |_|\___|_|_|\___/      \/  \/ \___/|_|  |_|\__,_|</code>
        <br/>
        <p>Here is ing.</p>
      </header>
    </div>
  );
}

export default App;
